package ru.korkmasov.tsc.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.dto.ITaskRecordRepository;
import ru.korkmasov.tsc.dto.TaskRecord;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRecordRepository extends AbstractRecordRepository<TaskRecord> implements ITaskRecordRepository {

    public TaskRecordRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public TaskRecord findById(@Nullable final String id) {
        return entityManager.find(TaskRecord.class, id);
    }

    public void remove(final TaskRecord entity) {
        TaskRecord reference = entityManager.getReference(TaskRecord.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        TaskRecord reference = entityManager.getReference(TaskRecord.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<TaskRecord> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskDto e", TaskRecord.class).getResultList();
    }

    @Override
    public List<TaskRecord> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<TaskRecord> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId",
                        TaskRecord.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void bindTaskToProjectById(String userId, String taskId, String projectId) {
        entityManager
                .createQuery("UPDATE TaskDto e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(String userId, String id) {
        entityManager
                .createQuery("UPDATE TaskDto e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public TaskRecord findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery(
                                "SELECT e FROM TaskDto e WHERE e.id = :id AND e.userId = :userId",
                                TaskRecord.class
                        )
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public TaskRecord findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery(
                                "SELECT e FROM TaskDto e WHERE e.name = :name AND e.userId = :userId",
                                TaskRecord.class
                        )
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public TaskRecord findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskRecord.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskDto e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.id =:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}