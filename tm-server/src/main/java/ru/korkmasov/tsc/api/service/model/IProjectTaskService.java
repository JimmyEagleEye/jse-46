package ru.korkmasov.tsc.api.service.model;

import ru.korkmasov.tsc.model.TaskGraph;

import java.util.List;

public interface IProjectTaskService {

    List<TaskGraph> findTaskByProjectId(String userId, final String projectId);

    void bindTaskById(String userId, final String taskId, final String projectId);

    void unbindTaskById(String userId, final String taskId);

    void removeProjectById(String userId, final String projectId);

    void removeProjectByIndex(String userId, final Integer index);

    void removeProjectByName(String userId, final String name);

}
