package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.AbstractGraph;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractGraph> {

    List<E> findAll();

    void add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

    E getReference(final String id);

}