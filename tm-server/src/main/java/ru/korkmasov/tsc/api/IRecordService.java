package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.dto.AbstractRecord;

public interface IRecordService<E extends AbstractRecord> extends IRecordRepository<E> {
}
