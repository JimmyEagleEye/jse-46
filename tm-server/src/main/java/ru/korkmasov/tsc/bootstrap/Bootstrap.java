package ru.korkmasov.tsc.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.IPropertyService;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.api.service.ILogService;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.api.service.dto.*;
import ru.korkmasov.tsc.api.service.model.IProjectService;
import ru.korkmasov.tsc.api.service.model.ISessionService;
import ru.korkmasov.tsc.api.service.model.ITaskService;
import ru.korkmasov.tsc.api.service.model.IUserService;
import ru.korkmasov.tsc.component.Backup;
import ru.korkmasov.tsc.dto.ProjectRecord;
import ru.korkmasov.tsc.dto.TaskRecord;
import ru.korkmasov.tsc.dto.UserRecord;
import ru.korkmasov.tsc.endpoint.*;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.service.ConnectionService;
import ru.korkmasov.tsc.service.DataService;
import ru.korkmasov.tsc.service.LogService;
import ru.korkmasov.tsc.service.PropertyService;
import ru.korkmasov.tsc.service.dto.*;
import ru.korkmasov.tsc.service.model.ProjectGraphService;
import ru.korkmasov.tsc.service.model.SessionGraphService;
import ru.korkmasov.tsc.service.model.TaskGraphService;
import ru.korkmasov.tsc.service.model.UserGraphService;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.korkmasov.tsc.util.SystemUtil.getPID;
import static ru.korkmasov.tsc.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskRecordService taskRecordService = new TaskRecordService(connectionService);

    @NotNull
    private final IProjectRecordService projectRecordService = new ProjectRecordService(connectionService);

    @NotNull
    private final IProjectTaskRecordService projectTaskRecordService = new ProjectTaskRecordService(connectionService);

    @NotNull
    private final IUserRecordService userRecordService = new UserRecordService(connectionService, propertyService);

    @NotNull
    private final ISessionRecordService sessionRecordService = new SessionRecordService(connectionService, userRecordService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskGraphService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectGraphService(connectionService);

    @NotNull
    private final IUserService userService = new UserGraphService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionGraphService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userRecordService, taskRecordService, projectRecordService, sessionRecordService);

    @NotNull
    private final ILogService logService = new LogService();

    //@NotNull
    //private final Backup backup = new Backup(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectRecordService, projectTaskRecordService, projectService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionRecordService, userRecordService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskRecordService, projectTaskRecordService, taskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userRecordService, userService, sessionRecordService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void start(String... args) {
        displayWelcome();
        process();
        //backup.init();
        initEndpoint();
    }

    public void initApplication() {
        initPID();
    }

    private void initData() {
        final String admin = userRecordService.add("admin", "admin", "admin@a").getId();
        @NotNull final UserRecord user = userRecordService.add("user", "user");

        projectRecordService.add(admin, new ProjectRecord("Project 1", "-")).setStatus(Status.COMPLETED);
        projectRecordService.add(admin, new ProjectRecord("Project B", "-"));
        projectRecordService.add(admin, new ProjectRecord("Project C", "-")).setStatus(Status.IN_PROGRESS);
        projectRecordService.add(admin, new ProjectRecord("Project D", "-")).setStatus(Status.COMPLETED);
        taskRecordService.add(admin, new TaskRecord("Task D", "-")).setStatus(Status.COMPLETED);
        taskRecordService.add(admin, new TaskRecord("Task C", "-"));
        taskRecordService.add(admin, new TaskRecord("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskRecordService.add(admin, new TaskRecord("Task A", "-")).setStatus(Status.COMPLETED);
    }


    public void initEndpoint() {
        initEndpoint(projectEndpoint);
        initEndpoint(sessionEndpoint);
        initEndpoint(taskEndpoint);
        initEndpoint(adminEndpoint);
        initEndpoint(dataEndpoint);
    }

    public void initEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";

    }

}
