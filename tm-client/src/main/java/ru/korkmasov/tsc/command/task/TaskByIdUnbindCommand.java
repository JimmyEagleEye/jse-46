package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(serviceLocator.getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskEndpoint().unbindTaskById(serviceLocator.getSession(), taskId);
    }

}
