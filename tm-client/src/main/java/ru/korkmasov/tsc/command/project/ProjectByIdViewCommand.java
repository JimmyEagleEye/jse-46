package ru.korkmasov.tsc.command.project;

import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.command.project.AbstractProjectCommand;
import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;

import ru.korkmasov.tsc.util.TerminalUtil;

public class ProjectByIdViewCommand extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(serviceLocator.getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
        //showProject(project);
    }

}
